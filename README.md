# Inspection de l'utilisation des ressources du pod


------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

Dans cet atelier, nous allons parler de l'inspection de l'utilisation des ressources des pods. 

- Nous allons discuter du serveur de métriques Kubernetes. 
- Nous parlerons de la commande `kubectl top`, 
- Démo : nous ferons une brève démonstration pratique de l'installation du serveur de métriques Kubernetes et de l'utilisation de `kubectl top` pour recueillir des informations sur les ressources utilisées par nos pods.

# Serveur de Métriques Kubernetes
Pour voir les métriques sur l'utilisation des ressources de nos pods et conteneurs, nous avons besoin d'un **add-on** pour **collecter** et **fournir ces données**. Un tel **add-on** est le serveur de métriques Kubernetes. Pour obtenir ces données, nous devons l'installer.

# La commande `kubectl top`

Une fois **l'add-on** installé, nous pouvons visualiser les données avec la commande `kubectl top`. Cette commande nous permet de voir les données sur l'utilisation des ressources dans nos pods et nous pouvons même voir l'utilisation des ressources par nœud également.

 `kubectl top` prend également en charge des options similaires à `kubectl get`, telles que les options `--sort-by` et `--selector`.

 exemple : 

 ```bash
 $ kubectl top pod --sort-by <JSONPATH> --selector <selector>
 ```

# Démo

[Consultez le lab relatif à cet atelier en suivant ce lien](https://gitlab.com/CarlinFongang-Labs/kubernetes/gestion-des-objets-kubernetes/lab-inspection-de-lutilisation-des-ressources-de-pod.git)


### Résumé du Laboratoire

Cet atelier a couvert la notion relatif au serveur de métriques Kubernetes, ainsi que l'usage de la commande `kubectl top`, et une lab pratique de l'installation du solution de métriques et de l'utilisation de `kubectl top`. Cela conclut notre atelier  de l'inspection de l'utilisation des ressources de nos pods.


# Reférences


[Outils de surveillance des ressources](https://kubernetes.io/docs/tasks/debug/debug-cluster/resource-usage-monitoring/)

[Aide mémoire kubectl](https://kubernetes.io/docs/reference/kubectl/quick-reference/#interacting-with-running-pods)